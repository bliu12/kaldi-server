FROM ubuntu:latest
MAINTAINER Barry Liu <bliu12@ncsu.edu>

# python 2 is installed because it is required to build Kaldi
RUN export DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get -y install apt-utils && apt-get install -y  \
    autoconf \
    automake \
    bzip2 \
    g++ \
    git \
    git-lfs \
    gfortran \
    libgstreamer-plugins-base1.0-dev \
    libgstreamer1.0 \
    libgstreamer-plugins-base1.0-0 \
    gstreamer1.0-plugins-good \
    gstreamer1.0-tools \
    gstreamer1.0-pulseaudio \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-ugly  \
    libatlas3-base \
    libgstreamer1.0-dev \
    libtool-bin \
    make \
    python2.7 \
    python3 \
    python3-pip \
    python3-yaml \
    python3-simplejson \
    python3-gi \ 
    subversion \
    unzip \
    wget \
    nano \
    build-essential \
    python3-dev \
    sox \
    zlib1g-dev && \
    pip3 install ws4py==0.3.2 && \
    pip3 install tornado==4.5.3 && \  
    ln -s /usr/bin/python2.7 /usr/bin/python ; ln -s -f bash /bin/sh && \
    pkg-config --cflags gstreamer1.0
